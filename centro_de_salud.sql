-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-11-2017 a las 04:33:14
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `centro_de_salud`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expediente_fam`
--

CREATE TABLE `expediente_fam` (
  `titular_fam` varchar(45) NOT NULL,
  `domicilio` varchar(80) DEFAULT NULL,
  `unidad_salud` int(11) DEFAULT NULL,
  `medico` int(11) NOT NULL,
  `nbss` int(11) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `bandera` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expediente_ind`
--

CREATE TABLE `expediente_ind` (
  `id_exind` int(11) NOT NULL,
  `expediente_fam` varchar(45) NOT NULL,
  `unidad_salud` int(11) DEFAULT NULL,
  `domicilio` varchar(45) DEFAULT NULL,
  `bandera` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_ind`
--

CREATE TABLE `historial_ind` (
  `id_exind` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `peso` float DEFAULT NULL,
  `estatura` float DEFAULT NULL,
  `temperatura` float DEFAULT NULL,
  `pulso` float DEFAULT NULL,
  `frec_resp` float DEFAULT NULL,
  `tension_sistolica` float DEFAULT NULL,
  `tension_diastolica` float DEFAULT NULL,
  `historial_indcol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `expediente_ind` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `app` varchar(45) DEFAULT NULL,
  `apm` varchar(45) DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `fecha_nace` date DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_medico`
--

CREATE TABLE `personal_medico` (
  `id_personal` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 NOT NULL,
  `app` varchar(45) CHARACTER SET utf8 NOT NULL,
  `apm` varchar(45) CHARACTER SET utf8 NOT NULL,
  `sexo` char(1) COLLATE utf8_spanish_ci DEFAULT NULL,
  `rfc` varchar(45) CHARACTER SET utf8 NOT NULL,
  `cedula` varchar(45) CHARACTER SET utf8 NOT NULL,
  `fecha_nace` date NOT NULL,
  `fecha_alta` date NOT NULL,
  `direccion` varchar(80) CHARACTER SET utf8 NOT NULL,
  `cargo` varchar(45) CHARACTER SET utf8 NOT NULL,
  `telefono` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `bandera` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `personal_medico`
--

INSERT INTO `personal_medico` (`id_personal`, `nombre`, `app`, `apm`, `sexo`, `rfc`, `cedula`, `fecha_nace`, `fecha_alta`, `direccion`, `cargo`, `telefono`, `bandera`) VALUES
(1, 'Jorge Luis', 'Hernandez ', 'Velasco', 'M', 'HEVJ920901-HQ2', 'SID32RG', '1992-09-01', '2017-10-10', 'Luis Donaldo Colosio 102, San Agustín de las Juntas', 'Administrador', '9513548521', 1),
(2, 'Claudia Araceli', 'Lopez', 'Rios', 'F', 'HSDHASF242', 'FSPFHE3', '1995-08-08', '2017-11-03', 'Col. Volcanes 123', 'Medico', '9512376528', 1),
(3, 'Yulissa Itzel', 'Martinez ', 'Santiago', 'F', 'IOSIOD26HH', 'GSDDUOAS', '1995-06-18', '2017-11-10', 'Conocida', 'Enfermera', '9518925637', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `nombre` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `contrasenia` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `fecha_alta` date DEFAULT NULL,
  `tipo_usuario` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `bandera` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_personal`, `nombre`, `contrasenia`, `fecha_alta`, `tipo_usuario`, `bandera`) VALUES
(1, 1, 'jorgeluis', 'admin2017', '2017-10-11', 'Administrador', 1),
(2, 3, 'yulissa', 'enfermera2017', '2017-11-11', 'Enfermera', 1),
(3, 2, 'claudia', 'medico2017', '2017-11-09', 'Medico', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `expediente_fam`
--
ALTER TABLE `expediente_fam`
  ADD PRIMARY KEY (`titular_fam`),
  ADD KEY `fk_expediente_fam_personal_medico1_idx` (`medico`);

--
-- Indices de la tabla `expediente_ind`
--
ALTER TABLE `expediente_ind`
  ADD PRIMARY KEY (`id_exind`),
  ADD KEY `fk_expediente_ind_expediente_fam1_idx` (`expediente_fam`);

--
-- Indices de la tabla `historial_ind`
--
ALTER TABLE `historial_ind`
  ADD KEY `fk_historial_ind_expediente_ind1_idx` (`id_exind`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD KEY `fk_paciente_expediente_ind1_idx` (`expediente_ind`);

--
-- Indices de la tabla `personal_medico`
--
ALTER TABLE `personal_medico`
  ADD PRIMARY KEY (`id_personal`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `id_personal_idx` (`id_personal`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `expediente_ind`
--
ALTER TABLE `expediente_ind`
  MODIFY `id_exind` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `personal_medico`
--
ALTER TABLE `personal_medico`
  MODIFY `id_personal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `expediente_fam`
--
ALTER TABLE `expediente_fam`
  ADD CONSTRAINT `fk_expediente_fam_personal_medico1` FOREIGN KEY (`medico`) REFERENCES `personal_medico` (`id_personal`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `expediente_ind`
--
ALTER TABLE `expediente_ind`
  ADD CONSTRAINT `fk_expediente_ind_expediente_fam1` FOREIGN KEY (`expediente_fam`) REFERENCES `expediente_fam` (`titular_fam`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historial_ind`
--
ALTER TABLE `historial_ind`
  ADD CONSTRAINT `fk_historial_ind_expediente_ind1` FOREIGN KEY (`id_exind`) REFERENCES `expediente_ind` (`id_exind`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `fk_paciente_expediente_ind1` FOREIGN KEY (`expediente_ind`) REFERENCES `expediente_ind` (`id_exind`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `id_personal` FOREIGN KEY (`id_personal`) REFERENCES `personal_medico` (`id_personal`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
