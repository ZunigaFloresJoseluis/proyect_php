 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Administrador</title>
      <link href="bootstrap.min.css" rel="stylesheet">
  <link href="main.css" rel="stylesheet">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>  
      <link rel="shortcut icon"  href="centro_de.ico"/>

       <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
        <script>
angular.module('myApp', []).controller('namesCtrl', function($scope, $http) {
$http.get("buscexpind.php")
   .then(function (response) {$scope.names = response.data.datos; $scope.mirespuesta=null;});    
});
</script>
   </head>

   <body>
         <div align="center">
         <img src="img/22901321_1603825223016187_1636282144_n.png" width="424" height="87" alt=""/>
         <img src="img/22854817_1604003049665071_464864409_n.png" width="111" height="99" alt=""/>   
         <img src="img/22894908_1604000879665288_1174307235_n.png" width="148" height="76" alt=""/><img src="img/22854490_1604000166332026_1456017282_n.png" width="239" height="106" alt=""/> 
         </div>
<div class="container-fluid" align="center" style="background-color: #827BE1;">
    <div class="container">
        <div class="col-md-12 text-center" style="color:#FFF; -webkit-font-smoothing: antialiased; padding: 5px;"> "Centro de Salud de San Agustin de las Juntas, Oaxaca de Juarez, Oax." </div>
    </div>
</div>
      
              <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- vinculo a bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Temas-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login--> 
      <link rel="stylesheet" type="text/css" href="estilos.css">
      <link rel="stylesheet" type="text/css" href="style.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body>
    <nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header" class="dropdown">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="administrador.php">Inicio</a>
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Personal<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="alta_p.php">Alta Personal</a></li>
          <li><a href="ver_p.php">Ver</a></li>
          <li><a href="baja_p.php">Personal inactivo</a></li>
        </ul>
      </li>
    </ul>
 
     <ul class="nav navbar-nav">
      
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Usuarios<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="alta_u.php">Alta Usuarios</a></li>
          <li><a href="ver_u.php">Ver</a></li>          
          <li><a href="baja_u.php">Usuarios inactivos</a></li>
         </ul>
      </li>
    </ul>

     <ul class="nav navbar-nav">
      
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Expedientes<b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="busqueda.php">Busqueda</a></li>
        </ul>
      </li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
        <li><a href="logout.php"><span class="glyphicon glyphicon-user"></span> Cerrar Sesion</a></li>
      </ul>
  </div>
</nav>
<h2>Busqueda de expedientes Individuales</h2>

<div ng-app="myApp" ng-controller="namesCtrl">
  <div class="container" align="center">
  <div class="row">
         <div id="custom-search-input">
                  <div class="input-group col-md-6">
                       <input type="text" ng-model="test" class="  search-query form-control" placeholder="Nombre de familia" />
                          <span class="input-group-btn">
                            <button class="btn btn-danger" type="button">
                          <span class=" glyphicon glyphicon-search"></span>
                             </button>
                          </span>
                   </div>
             </div>
  </div>
</div>
<div><p>res: {{mirespuesta}}</p></div>
<div class="table-responsive">
    <table class="table">
    <thead>
      <tr >
        <th>ID</th>
        <th>Exp. Familiar</th>
        <th>Nombre</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Fecha Alta</th>
        <th>Fecha Nacimiento</th>
        <th>Genero</th>
        <th>Unidad de Salud</th>
        <th>Domicilio</th>
        <th colspan="2">Acciones </th>
      </tr>
      
          <tr ng-repeat="x in datos  | filter:test">
            
            <h3> <?php echo $outp.datos ?></h3>
           <td>{{ x.expediente_fam}}</td> 
           <td>{{ x.expediente_fam}}</td>   
            <td>{{ x.nombre}}</td>
            <td>{{ x.app}}</td>
            <td>{{ x.apm}}</td>
            <td>{{ x.fecha_alta}}</td>
            <td>{{ x.fecha_nac}}</td>
            <td>{{ x.sexo}}</td>
            <td>{{ x.unidad_salud}}</td>
            <td>{{ x.domicilio}}</td>


                      
            <td><a href="ver_historial.php.php?id_exind={{x.id_exind}}">Ver Historial</td>
            <td><a href="eliminar_exp_ind.php.php?id_exind={{x.id_exind}}">Desactivar</a></td>
            </tr>    
     
    </thead>
    <tbody>
    </tbody>
  </table>
</div>


<div class="mastfoot" align="center" style="background-color: #827BE1 ">
      <p>Direccion: Porfirio Diaz 1348, San Agustin de las Juntas, Centro Oaxaca</p>
</div>
          
 <!-- vinculando a libreria Jquery-->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <!-- Libreria java scritp de bootstrap -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 

<script>
  

</script>

  </body>

</html>

<?php }}
           else
           { 
                  ?>

                    <script>
                               alert("Acceso restringido, Ingrese correctamente");
                               window.location = 'index.html';
                    </script>
          <?php } ?>          
