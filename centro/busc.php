<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
require("configuracion.php");

$result = mysqli_query($conexion,"SELECT num_expediente,titular_fam, domicilio,unidad_salud, medico,nbss,telefono FROM expediente_fam where bandera=1");

$outp = "";
while($rs = mysqli_fetch_array($result))
{
    if($outp != "") 
    {
    	$outp .= ",";
    }

    $outp .= '{"num_expediente":"'.$rs["num_expediente"].'",';
     $outp .= '"titular_fam":"'. $rs["titular_fam"].'",';
    $outp .= '"domicilio":"'. $rs["domicilio"].'",';
    $outp .= '"unidad_salud":"'.$rs["unidad_salud"].'",';
    $outp .= '"medico":"'.$rs["medico"].'",';
    $outp .= '"telefono":"'.$rs["telefono"].'",';
    $outp .= '"nbss":"'.$rs["nbss"].'"}';
    
}
$outp ='{"datos":['.$outp.']}';

echo($outp);
?>