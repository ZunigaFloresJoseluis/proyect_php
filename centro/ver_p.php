<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("configuracion.php");
include("menu.php");
error_reporting(E_ALL ^ E_NOTICE); //no mostrar errores de sintaxis
if($_SESSION["tipo_usuario"]  == "Administrador")
{

?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Administrador</title>
      <link href="bootstrap.min.css" rel="stylesheet">
      <link href="main.css" rel="stylesheet">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>  
      <link rel="shortcut icon"  href="centro_de.ico"/>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
        <script>
angular.module('myApp', []).controller('namesCtrl', function($scope, $http) {
$http.get("buscpe.php")
   .then(function (response) {$scope.names = response.data.datos; $scope.mirespuesta=null;});    
});
</script>
     

              <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- vinculo a bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Temas-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
  <link rel="stylesheet" type="text/css" href="estilos.css">
  <link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

   </head>

   <body>
        
      
   
   
<h2>Ver personal</h2>
<div ng-app="myApp" ng-controller="namesCtrl">
<div class="container" align="center">
  <div class="row">
               <div id="custom-search-input">
                  <div class="input-group col-md-6">
                       <input type="text" ng-model="test" class="  search-query form-control" placeholder="Buscar personal" />
                          <span class="input-group-btn">
                            <button class="btn btn-danger" type="button">
                          <span class=" glyphicon glyphicon-search"></span>
                             </button>
                          </span>
                   </div>
             </div>
  </div>
</div>
<div class="table-responsive" >
   <table class="table">
    <thead>
      <tr >
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellido P</th>
        <th>Apellido M</th>
        <th>Género</th>
       <!-- <th>RFC</th>
        <th>Cédula</th>-->
        <th>F. Nacimiento</th>
        <th>F. Alta</th>
        <th>Dirección</th>
        <th>Cargo</th>
        <th>Teléfono</th>
        <th colspan="2">Acciones</th>
      </tr>
        <tr ng-repeat="x in names | filter:test">
            <td>{{ x.id_personal}}</td>
            <td>{{ x.nombre}}</td>
            <td>{{ x.app }}</td>
            <td>{{ x.apm }}</td>
            <td>{{ x.sexo }}</td>
           <!-- <td>{{ x.rfc}}</td>
            <td>{{ x.cedula}}</td>-->
            <td>{{ x.fecha_nace}}</td>
            <td>{{ x.fecha_alta}}</td>
            <td>{{ x.direccion}}</td>
             <td>{{ x.cargo}}</td>
               <td>{{ x.telefono}}</td>-->


                      
            <td><a href="editar_personal.php?id_personal={{x.id_personal}}">Editar</a></td>
            <td><a href="eliminar_personal.php?id_personal={{x.id_personal}}">Desactivar</a></td>
          
            </tr>

    </thead>
    <tbody>
    </tbody>
  </table>
</div>
</div>
<div class="mastfoot" align="center" style="background-color: #827BE1 ">
      <p>Dirección: Porfirio Díaz 1348, San Agustín de las Juntas, Centro Oaxaca</p>
</div>
 <!-- vinculando a libreria Jquery-->

  </body>

</html>

<?php }
           else
           { 
                  ?>

                    <script>
                               //alert("Acceso restringido, Ingrese correctamente");
                               //window.location = 'index.html';
                    </script>
          <?php } ?>          
