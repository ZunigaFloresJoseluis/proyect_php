<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("configuracion.php");
include("menu.php");
error_reporting(E_ALL ^ E_NOTICE); //no mostrar errores de sintaxis
if($_SESSION["tipo_usuario"] == "Administrador")
{

if (strtolower($_REQUEST["act"]) == "actualizar")
  {
    if($_REQUEST["nombre"] == "" or $_REQUEST["app"] == "" or  $_REQUEST["apm"] == "" or $_REQUEST["sexo"] == "" or $_REQUEST["rfc"] == ""  or $_REQUEST["cedula"] == "" or $_REQUEST["fecha_nace"] == "" or $_REQUEST["direccion"] == "" or $_REQUEST["cargo"] == "" or $_REQUEST["cargo"] == "Seleccionar cargo" or $_REQUEST["telefono"] == "" )
    {   
      ?>
        <script language="javascript">
        alert("\tRellena los Campos Correctamente \n \tFavor de verificar");
        window.location="ver_p.php";
      </script>           
      <?php
    }
  
       else 
       {
          
          $act = "UPDATE personal_medico set nombre = '".$_REQUEST["nombre"]."', app ='".$_REQUEST["app"]."',apm  = '".$_REQUEST["apm"]."', sexo='".$_REQUEST["sexo"]."',rfc='".$_REQUEST["rfc"]."',  cedula='".$_REQUEST["cedula"]."',fecha_nace='".$_REQUEST["fecha_nace"]."',direccion='".$_REQUEST["direccion"]."', cargo='".$_REQUEST["cargo"]."', telefono='".$_REQUEST["telefono"]."' WHERE id_personal = ".$_REQUEST["id_2"]."";
         if(mysqli_query($conexion,$act))
     {   
      ?>
        <script language="javascript">
    alert("Actualizado Correctamente");
    window.location='ver_p.php';
    </script>
      <?php    
     }
     else
    {
      echo mysqli_error($conexion);
    }
  }
}
  if($_REQUEST["id_personal"] != "")
  {
    $consulta = mysqli_query($conexion,"SELECT * FROM personal_medico WHERE id_personal = ".$_REQUEST["id_personal"]."");
  $mostrar = mysqli_fetch_array($consulta);
    
  if(mysqli_num_rows($consulta) >= 1) // checa que la consulta refleje registro(s)
    {
      $nombre = $mostrar["nombre"];
      $app = $mostrar["app"];    
      $apm = $mostrar["apm"];
     // $sexo = $mostrar["sexo"];
      $rfc = $mostrar["rfc"];
      $cedula = $mostrar["cedula"];
     // $fecha_nace = $mostrar["fecha_nace"];
      $direccion = $mostrar["direccion"];
      $cargo = $mostrar["cargo"];
      $telefono = $mostrar["telefono"];

?>

 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Administrador</title>
      <link href="bootstrap.min.css" rel="stylesheet">
  <link href="main.css" rel="stylesheet">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>  
       <link rel="shortcut icon"  href="centro_de.ico"/>
        
      
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- vinculo a bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Temas-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login--> 
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="stylesheet" type="text/css" href="style.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body>

<h2>Editar personal</h2>
<div class="container-fluid" id="formulario">
   <form action="editar_personal.php" class="form-horizontal" method="post" enctype="multipart/form-data">
     <input type="hidden" name="id_2" value="<?php echo $_REQUEST["id_personal"]; ?>">
    <div class="form-group">

        <label class="col-xs-2 control-label"><label style="color: red">*</label>Nombre:</label>
        <div class="col-xs-9">
            <input type="text" name="nombre" class="form-control"  value="<?php echo $nombre?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2 control-label"><label style="color: red">*</label>Apellido paterno:</label>
        <div class="col-xs-9">
            <input type="text" name="app" class="form-control" value="<?php echo $app?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2 control-label">Apellido Materno:</label>
        <div class="col-xs-9">
            <input type="text" name="apm" class="form-control" value="<?php echo $apm?>">
        </div>
    </div>
     <div class="form-group">
        <label class="col-xs-2 control-label"><label style="color: red">*</label>F. Nacimiento:</label>
        <div class="col-xs-9">
            <input type="date" name="fecha_nace" class="form-control">
        </div>
    </div>
     <div class="form-group">
        <label class="control-label col-xs-2"><label style="color: red">*</label>Genero:</label>
        <div class="col-xs-4">
            <label class="radio-inline">
                <input type="radio" name="sexo" value="M"> Masculino
            </label>
        </div>
        <div class="col-xs-2">
            <label class="radio-inline">
                <input type="radio" name="sexo" value="F"> Femenino
            </label>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2 control-label"><label style="color: red">*</label>Rfc:</label>
        <div class="col-xs-9">
            <input type="text" name="rfc" class="form-control" value="<?php echo $rfc?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2 control-label"><label style="color: red">*</label>Cedula:</label>
        <div class="col-xs-9">
            <input type="text" name="cedula" class="form-control" value="<?php echo $cedula?>">
        </div>
    </div>  
    <div class="form-group">
      <label class="control-label col-xs-2" ><label style="color: red">*</label>Cargo:</label>
          <div class="col-xs-9">
          <select name= "cargo">
          <option >Seleccionar cargo</option>
          <option value="Administrador">Administrador</option>
          <option value="Enfermera">Enfermero(a)</option>
          <option value="Medico">Medico</option>
        </select>
        </div>
      </div>
    <div class="form-group">
        <label class="col-xs-2 control-label" >Telefono:</label>
        <div class="col-xs-9">
            <input type="tel" name="telefono" onkeypress="return numeros (event)" class="form-control" value="<?php echo $telefono?>">
        </div>
    </div>
       <div class="form-group">
        <label class="control-label col-xs-2">Dirección:</label>
        <div class="col-xs-9">
            <input type="text" name="direccion" class="form-control" value="<?php echo $direccion?>"></textarea>
        </div>
    </div>
      <div align="left">
          <br>
          <br>
         <label><label style="color: red">*</label>Campos Obligatorios</label>
        </div>
       <div class="form-group">
        <br>
        <div class="col-xs-offset-2 col-xs-9">
            <input type="submit" class="btn btn-primary" name="act" value="Actualizar">
        </div>
    </div>
</form>
</div>
<br>

     
 <!-- vinculando a libreria Jquery-->


   <script>
  function numeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " 0123456789";
    especiales = [8,37,39,46];
 
    tecla_especial = false
    for(var i in especiales){
 if(key == especiales[i]){
     tecla_especial = true;
     break;
        } 
    }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}
</script>
  </body>

</html>

<?php 
}
}
}
           else
           { 
                  ?>

                    <script>
                              // alert("Acceso restringido, Ingrese correctamente");
                               //window.location = 'index.html';
                    </script>
          <?php } ?>          
