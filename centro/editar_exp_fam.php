<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
include("configuracion.php");
include("menu.php");
error_reporting(E_ALL ^ E_NOTICE); //no mostrar errores de sintaxis
if($_SESSION["tipo_usuario"] == "Administrador")
{

if ($_REQUEST["actualizar"])
  {
    if($_REQUEST["titular_fam"] == "" or $_REQUEST["domicilio"] == ""  or $_REQUEST["medico"] == "Seleccionar"  or $_REQUEST["telefono"] == "" )
    {   
      ?>
        <script language="javascript">
        alert("\tRellena los Campos Correctamente \n \tFavor de verificar");
        window.location="editar_exp_fam.php";
      </script>           
      <?php
    }
  
       else 
       {
          
          $act = "UPDATE expediente_fam set titular_fam = '".$_REQUEST["titular_fam"]."', domicilio='".$_REQUEST["domicilio"]."', telefono='".$_REQUEST["telefono"]."' WHERE num_expediente = ".$_REQUEST["id_2"]."";
         if(mysqli_query($conexion,$act))
     {   
      ?>
        <script language="javascript">
    alert("Actualizado Correctamente");
    window.location='busqueda.php';
    </script>
      <?php    
     }
     else
    {
      echo mysqli_error($conexion);
    }
  }
}
 
 if($_REQUEST["num_expediente"] != "")
  {
    $consulta = mysqli_query($conexion,"SELECT * FROM expediente_fam WHERE num_expediente = ".$_REQUEST["num_expediente"]."");
  $mostrar = mysqli_fetch_array($consulta);
    
  if(mysqli_num_rows($consulta) >= 1) // checa que la consulta refleje registro(s)
    {
      $titular_fam = $mostrar["titular_fam"];
      $domicilio = $mostrar["domicilio"];    
      $medico = $mostrar["medico"];


      $telefono= $mostrar["telefono"];

    

?>
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
     <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Administrador</title>
      <link href="bootstrap.min.css" rel="stylesheet">
  <link href="main.css" rel="stylesheet">
  <script src="jquery.min.js"></script>
  <script src="bootstrap.min.js"></script>  
      <link rel="shortcut icon"  href="centro_de.ico"/>
     

              <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <!-- vinculo a bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Temas-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- se vincula al hoja de estilo para definir el aspecto del formulario de login-->  
      <link rel="stylesheet" type="text/css" href="estilos.css">
      <link rel="stylesheet" type="text/css" href="style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   <script src="validator.js"></script>

    </head>
    <body>
<h2>Actualización de datos en expediente Familiar </h2>
<?php
  
  $query = 'SELECT nombre FROM personal_medico WHERE cargo ="Medico"';
  $result = $conexion->query($query);

 ?>

<div class="container" id="formulario">
<form action="editar_exp_fam.php" class="form-horizontal" data-toggle="validator" role="form" >

   <input type="hidden" name="id_2" value="<?php echo $_REQUEST["num_expediente"]; ?>">
    <div class="form-group">
        <label class="col-xs-2 control-label">Familia:</label>
        <div class="col-xs-9">
            <input type="text" class="form-control" name="titular_fam" value="<?php echo $titular_fam?>" required>
        </div>
    </div>
 <div class="form-group">
        <label class="col-xs-2 control-label">Domicilio:</label>
        <div class="col-xs-9">
            <input type="text" class="form-control" name="domicilio" value="<?php echo $domicilio?>" required>
        </div>
    </div>



     <div class="form-group">
        <label class="col-xs-2 control-label" >Teléfono:</label>
        <div class="col-xs-9">
            <input type="text" class="form-control" name="telefono" onkeypress="return numeros (event)" value="<?php echo $telefono?>" required>
        </div>
    </div>
  <br>
       <div class="form-group">
            <div class="col-xs-offset-2 col-xs-9">
            <input type="submit" class="btn btn-primary" name="actualizar" value="Actualizar">
           
        </div>
    </div>
</form>
</div>
</form>
</div>
<br>
<br>
 <footer class="container-fluid text-center">
 <p>Direccion: Porfirio Diaz 1348, San Agustin de las Juntas, Centro Oaxaca</p>
</footer>
        <script>
  function numeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " 0123456789";
    especiales = [8,37,39,46];
 
    tecla_especial = false;
    for(var i in especiales){
 if(key == especiales[i]){
     tecla_especial = true;
     break;
        } 
    }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
        return false;
}
</script>
>
  </body>

</html>

<?php }}}
           else
           { 
                  ?>

                    <script>
                               alert("Acceso restringido, Ingrese correctamente");
                               window.location = 'index.html';
                    </script>
          <?php } ?>          
